app.factory 'departmentsfactory', ($resource) ->
  console.log "factory data"
  return $resource dbServer+'/retrieveDepartments'

#app.service 'departmentService', ($resource) ->
#  @createNewDept = (deptName,departmentDescription) ->
#    console.log 'new department added'
#    $resource
#    .post(dbServer + '/addDepartment', params : {deptName:deptName,departmentDescription:departmentDescription})
#    .success((data, status, headers, config) ->
##      console.log data
#      return data
#    )
#    .error((data,status,headers,config) ->
#    )
#
#  @retrieveDepartments = () ->
#    console.log "resourse"
#    return departmentService.query()

#  @retrieveDepartments = () ->
#    console.log "depts"
#    $resource
#    .get(dbServer+'/retrieveDepartments')
#    .success((data,status,headers,config) ->
#      console.log data
#      return data
#    )
#    .error((data,status,headers,config) ->
#    )

#  @retrieveDepartmentById = (deptId) ->
#    $resource
#    .get(dbServer + '/retrieveDepartmentsById', params: {deptId:deptId})
#    .success((data,status,headers,config) ->
#      return data
#    )
#    .error((data,status,headers,config) ->
#    )
#
#  @editDepartmentDetails = (application, deptId) ->
#    $resource
#    .post(dbServer + 'editDepartments', {application:application, deptId:deptId})
#    .success((data, status, headers, config) ->
#      return data
#    )
#    .error((data,status,headers,config) ->
#    )
#
#  return

#app.controller 'AddDepartmentsCtrl', ($scope, $location, departmentService, $resource) ->
#  $scope.createNew = ->
#    departmentService.createNewDept($scope.deptName, $scope.departmentDescription).then (data) ->
#      if data.data.result
#        $window.location='#/adminDepartments'
#    return
#    $location.path('/adminDepartments')

app.controller 'DepartmentsCtrl', ($scope, departmentsfactory) ->
  console.log "resourse data"
  $scope.departments = departmentsfactory.query()
  console.log $scope.departments
#  departmentService.retrieveDepartments().then (data) ->
#    $scope.departments = data.data
#  return

  $scope.pageSize = 5
  $scope.pageSelected = 0

  $scope.maxPage = (departments)->
    if departments == undefined or departments.length < $scope.length
      return 1
    parseInt( (departments.length + $scope.pageSize - 1)/ $scope.pageSize, 10 )

  isPageValid  = (departments, page) ->
    page >= 0 && page < $scope.maxPage(departments)

  $scope.changePage = (departments, d) ->
    if isPageValid(departments, $scope.pageSelected + d)
      $scope.pageSelected += d

app.filter 'paginate', ->
  (arr, pageSize, pageSelected) ->
    arr.slice pageSize * pageSelected, pageSize * (1 + pageSelected)

#app.controller 'editDepartmentCtrl', ($scope, $param, departmentsService) ->
#  departmentsService.retrieveDepartmentById($param.deptId).then (data) ->
#    $scope.departments = {
#      deptName: data.data.deptName,
#      description: data.data.description
#    }
#    $scope.deptId = data.data.deptId
#  return