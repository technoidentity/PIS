app.factory 'levelsFactory', ($resource) ->
  return $resource(dbServer+'/retrieveLevelsList', {}, {
    query: {method: 'GET',isArray: true},
    create: {method: 'POST', url: dbServer + '/addLevel'},
    update: {method: 'POST', url: dbServer + '/updateLevel'},
    delete: {method: 'POST', url: dbServer + '/deleteLevel'}
  })

app.factory 'editLevelFactory', () ->
  LevelType = {}
  return {LevelType:LevelType}

app.controller 'LevelsCtrl', ($scope,editLevelFactory,levelsFactory,$filter,ngTableParams,$window) ->

  getLevels = () ->
    $scope.levelsTable = new ngTableParams({
      page: 1,
      count: 5,
      sorting: {
        levelCode: 'asc'
      }
    }, {
      total: $scope.Levels.length
      getData: ($defer, params) ->
        filteredData = if params.filter() then $filter("filter")($scope.Levels, params.filter()) else $scope.Levels
        orderedData = if params.sorting() then $filter("orderBy")(filteredData, params.orderBy()) else $scope.Levels
        params.total orderedData.length
        $defer.resolve orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count())
    })
  levelsFactory.query().$promise.then (data) ->
    $scope.Levels = data
    getLevels()

  $scope.LevelType = {}
  $scope.fetchLevel = (data) ->
    editLevelFactory.LevelType = {
      levelId: data.levelId,
      levelCode: data.levelCode,
      levelName: data.levelName,
      description: data.description
    }
    $window.location = '#/adminLevel/' + data.levelId

  $scope.createNew = ->
#    console.log 'adding...'
#    console.log $scope.levelName
    levelsFactory.create(angular.toJson({levelCode:$scope.levelCode, levelName:$scope.levelName, description:$scope.description})).$promise.then (data) ->
      if data.result
#        console.log "created"
        $window.location = '#/adminLevels'
      else
        console.log "not saved"

  $scope.update = (id) ->
    levelsFactory.update(angular.toJson(id:id)).$promise.then (data) ->
      if data.result
#      console.log "updated"
        $window.location = '#/adminLevels'

  $scope.delete = (id) ->
#    console.log id
    levelsFactory.delete(angular.toJson({id:id})).$promise.then (data) ->
      if data.result
#        console.log "deleted"
#        getLevels()
        $window.location.reload();

app.controller 'EditLevelCtrl', ($scope, levelsFactory, editLevelFactory, $window) ->
  $scope.LevelType = editLevelFactory.LevelType

  $scope.update = (id) ->
#    console.log "Id: " + id
#    console.log $scope.LevelType
    levelsFactory.update(angular.toJson(level:$scope.LevelType)).$promise.then (data) ->
      if data.result
#        console.log "updated"
        $window.location = '#/adminLevels'