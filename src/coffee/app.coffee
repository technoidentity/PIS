dbServer = 'http://0.0.0.0:1234'
#dbServer = 'http://192.168.0.175:1234'
#pg = require 'pg'
app = angular.module('PIS',['ui.router','ngTable', 'ngResource'])
app.config(($stateProvider) ->
  $stateProvider
    .state('login', {
      url: "",
      views: {
        "viewA": { templateUrl: "loginHeader.html" },
        "viewB": { controller:'LoginCtrl',templateUrl: "login.html" }
      }
    })
    .state('dashboard', {
      url: "/dashboard",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
#        "viewB": { controller:'LoanCtrl',templateUrl: "hr/loan.html" }
        "viewB": { templateUrl: "dashboard.html" }
      }
    })
    .state('hrLoan', {
      url: "/hrLoan",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'LoanCtrl',templateUrl: "hr/loan.html" }
      }
    })
    .state('hrOvertime', {
      url: "/hrOvertime",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'OvertimeCtrl',templateUrl: "hr/overtime.html" }
      }
    })
    .state('hrOvertimeEmp', {
      url: "/hrOvertime/:ApplicationID",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'OvertimeEmpCtrl',templateUrl: "hr/overtime_employee.html" }
      }
    })
    .state('hrAbsent', {
      url: "/hrAbsent",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'AbsentCtrl',templateUrl: "hr/absent.html" }
      }
    })
    .state('hrAbsentEmp', {
      url: "/hrAbsent/:ApplicationID",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'AbsentEmpCtrl',templateUrl: "hr/absent_employee.html" }
      }
    })
    .state('hrReimbursement', {
      url: "/hrReimbursement",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'ReimbursementCtrl',templateUrl: "hr/reimbursement.html" }
      }
    })
    .state('employeeProfile', {
      url: "/employeeProfile",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
#        "viewB": { controller:'ReimbursementCtrl',templateUrl: "hr/reimbursement.html" }
        "viewB": { templateUrl: "employee/profile.html" }
      }
    })
    .state('employeeAttendance', {
      url: "/employeeAttendance",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
#        "viewB": { controller:'ReimbursementCtrl',templateUrl: "hr/reimbursement.html" }
        "viewB": { templateUrl: "employee/attendance.html" }
      }
    })
    .state('leaveApply', {
      url: "/leaveApply",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'LeaveApplyCtrl',templateUrl: "employee/leave_apply.html" }
      }
    })
    .state('trackLeaves', {
      url: "/trackLeaves",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'LeaveApplyCtrl',templateUrl: "employee/track_leaves.html" }
      }
    })

  .state('applySupportingDoc', {
      url: "/supportDocument",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
#        "viewB": { controller:'ReimbursementCtrl',templateUrl: "hr/reimbursement.html" }
        "viewB": { templateUrl: "employee/supporting_docs.html" }
      }
    })

    .state('loanApply', {
      url: "/loanApply",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'LoanApplyCtrl',templateUrl: "employee/loan_apply.html" }
      }
    })
    .state('reimbursementApply', {
      url: "/reimbursementApply",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
#        "viewB": { controller:'ReimbursementCtrl',templateUrl: "hr/reimbursement.html" }
        "viewB": { templateUrl: "employee/reimbursement_apply.html" }
      }
    })
    .state('overtimeApply', {
      url: "/overtimeApply",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
#        "viewB": { controller:'ReimbursementCtrl',templateUrl: "hr/reimbursement.html" }
        "viewB": { templateUrl: "employee/overtime_apply.html" }
      }
    })
    .state('hrReimbursementEmp', {
      url: "/hrReimbursement/:ApplicationID",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'ReimbursementEmpCtrl',templateUrl: "hr/reimbursement_employee.html" }
      }
    })
    .state('hrAssignShift', {
      url: "/hrAssignShift",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'AssignShiftCtrl',templateUrl: "hr/assign_shift.html" }
      }
    })
    .state('hrAssignShiftEmp', {
      url: "/hrAssignShift/:ApplicationID",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'AssignShiftEmpCtrl',templateUrl: "hr/assign_shift_employee.html" }
      }
    })
    .state('hrLoanEmp', {
      url: "/hrLoan/:ApplicationID",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'LoanEmpCtrl',templateUrl: "hr/loan_employee.html" }
      }
    })
    .state('hrLeaveEmp', {
      url: "/hrLeave/:ApplicationID",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'LeaveEmpCtrl',templateUrl: "hr/leave_employee.html" }
      }
    })
    .state('hrManualAttendance', {
      url: "/hrManualAttendance",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'ManualAttendanceCtrl',templateUrl: "hr/manual_attendance.html" }
      }
    })
    .state('addEmployee', {
      url: "/addEmployee",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'addEmpCtrl',templateUrl: "hr/add_employee.html" }
      }
    })
    .state('manageEmployeeList', {
      url: "/manageEmployeeList",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
#        "viewB": { templateUrl: "hr/manage_employee_list.html" }
        "viewB": { controller:'manageEmployeeCtrl',templateUrl: "hr/manage_employee_list.html" }
      }
    })
  .state('manageEmployee', {
      url: "/manageEmployee/:ApplicationID",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'manageEmployeeCtrl',templateUrl: "hr/manage_employee.html" }
      }
    })
    .state('hrLeave', {
      url: "/hrLeave",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'LeaveCtrl',templateUrl: "hr/leave.html" }
      }
    })
  .state('hrSubstituteEmployee', {
      url: "/hrSubstituteEmployee",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'substituteEmployeeCtrl',templateUrl: "hr/substitute_employee.html" }
      }
    })
    .state('adminAddDept', {
      url: "/adminAddDepartments",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'AddDepartmentsCtrl',templateUrl: "admin/add_department.html" }
      }
    })
  .state('adminAddLevel', {
      url: "/adminAddLevel",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'LevelsCtrl',templateUrl: "admin/add_level.html" }
      }
    })
  .state('adminLevels', {
      url: "/adminLevels",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'LevelsCtrl',templateUrl: "admin/levels.html" }
      }
    })

  .state('adminEditLevel', {
      url: "/adminLevel/:levelId",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'EditLevelCtrl',templateUrl: "admin/edit_level.html" }
      }
    })

  .state('adminDeptlist', {
    url: "/adminDepartments",
    views: {
      "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
      "viewB": { controller:'DepartmentsCtrl',templateUrl: "admin/departments.html" }
    }
  })

  .state('adminEditDepartments', {
      url: "/adminAddDepartments/:deptId",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'editDepartmentCtrl',templateUrl: "admin/add_department.html" }
      }
    })

  .state('adminAddHoliday', {
    url: "/adminAddHoliday",
    views: {
      "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
      "viewB": { controller:'AddHolidayCtrl',templateUrl: "admin/add_holiday.html" }
    }
  })

  .state('adminHolidaysList', {
      url: "/adminHolidays",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'HolidaysCtrl',templateUrl: "admin/holidays.html" }
      }
    })

  .state('adminAddDesignation', {
    url: "/adminAddDesignation",
    views: {
      "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
      "viewB": { controller:'AddDesignationsCtrl',templateUrl: "admin/add_designation.html" }
    }
  })

  .state('designationsList', {
      url: "/adminDesignations",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'DesignationsCtrl',templateUrl: "admin/designations.html" }
      }
    })

  .state('adminEditDesignation', {
      url: "/adminAddDesignation/:desigId",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'addDepartmentCtrl',templateUrl: "admin/add_designation.html" }
      }
    })
    .state('adminAddShift', {
      url: "/adminAddShift",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'AddShiftCtrl',templateUrl: "admin/add_shift.html" }
      }
    })
  .state('adminAddLoan', {
      url: "/adminAddLoan",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'AddLoanCtrl',templateUrl: "admin/add_loan.html" }
      }
    })
  .state('adminLoansList', {
      url: "/adminLoans",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'LoansCtrl',templateUrl: "admin/loans.html" }
      }
    })
  .state('adminAddAbsent', {
      url: "/adminAddAbsent",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'AbsentsCtrl',templateUrl: "admin/add_absent.html" }
      }
    })

  .state('adminLeavesList', {
      url: "/adminAbsents",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'AbsentsCtrl',templateUrl: "admin/absents.html" }
      }
    })
  .state('adminEditLeaves', {
      url: "/adminLeave/:leaveId",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'AbsentsCtrl',templateUrl: "admin/edit_leave.html" }
      }
    })
  .state('adminAddAbsentCategory', {
      url: "/adminAddAbsentCategory",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'AbsentCategoriesCtrl',templateUrl: "admin/add_absent_category.html" }
      }
    })

  .state('adminAbsentCategories', {
      url: "/adminAbsentCategories",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'AbsentCategoriesCtrl',templateUrl: "admin/absent_categories.html" }
      }
    })
  .state('adminLeaveBenefits', {
      url: "/adminLeaveBenefits",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'',templateUrl: "admin/leave_benefits.html" }
      }
    })

  .state('adminCalendar', {
      url: "/adminCalendar",
      views: {
        "viewA": {  controller:'HeaderCtrl',templateUrl: "header.html" },
        "viewB": { controller:'',templateUrl: "admin/calendar.html" }
      }
    })
#    .otherwise(redirectTo : "/") /adminLeaves adminCalendar

  return
)